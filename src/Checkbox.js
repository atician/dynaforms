import React, { Component } from 'react';

class Checkbox extends Component {
  render() {
    const { label, name, onChange } = this.props
    return (
      <label>
        {label}
        <input type="checkbox" name={name} onChange={onChange}/>
      </label>
    );
  }
}

export default Checkbox;
