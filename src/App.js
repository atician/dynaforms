import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Form from './Form';
import * as validators from './utils/validators';

class App extends Component {
  render() {
    const fields = {
      byName: {
        'fullName': {
          name: 'fullName',
          label: 'Name',
          type: 'text',
          validate: form => validators.fullName(form.fullName)
        },
        'gender': {
          name: 'gender',
          label: 'Gender',
          type: 'select',
          options: [
            {
              label: 'Male',
              value: 0
            }, {
              label: 'Female',
              value: 1
            }
          ]
        },
        'consent': {
          name: 'consent',
          label: 'Require guardian consent',
          type: 'checkbox'
        },
        'guardian': {
          name: 'guardian',
          label: 'Guardian details',
          type: 'text',
          validate: form => (
            (
              validators.checkbox(form.consent)
                && validators.fullName(form.guardian)
            ) || !validators.checkbox(form.consent)
          )
        }
      },
      names: ['fullName', 'gender', 'consent', 'guardian']
    };
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Form fields={fields}/>
      </div>
    );
  }
}

export default App;
