import React, { Component } from 'react';

class SelectInput extends Component {
  render() {
    const { label, name, options, onChange } = this.props
    return (
      <label>
        {label}
        <select name={name} onChange={onChange}>
          <option/>
          {options.map(option => (
            <option key={option.value} value={option.value}>{option.label}</option>
          ))}
        </select>
      </label>
    );
  }
}

export default SelectInput;
