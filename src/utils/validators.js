
export const fullName = s => {
  if (typeof s == 'object' && typeof s.value == 'string') {
    const [first, last] = s.value.split(' ');
    if (first && last) {
      return true;
    }
  }
  return false;
}

export const checkbox = checkbox => {
  if (typeof checkbox == 'object' && typeof checkbox.value == 'boolean') {
    return checkbox.value;
  }
  return false;
}
