import React, { Component } from 'react';

class TextInput extends Component {
  render() {
    const { label, name, onChange } = this.props
    return (
      <label>
        {label}
        <input type="text" name={name} onChange={onChange}/>
      </label>
    );
  }
}

export default TextInput;
