import React, { Component } from 'react';
import TextInput from './TextInput';
import SelectInput from './SelectInput';
import Checkbox from './Checkbox';

// props = [
//   {
//     name: 'name',
//     type: 'text',
//     isRepeatable: false,
//     validation: () => ()
//   }
// ];

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleSubmit() {
    console.log(this.state);
    this.setState({'submitted': true});
  }
  handleChange(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const validate = this.props.fields.byName[name].validate;
    this.setState({
      [name]: {
        value,
        valid: validate ? validate(this.state) : true
      }
    });
  }
  render() {

    return (
      <div>
        {this.props.fields.names.map(name => {
          const field = this.props.fields.byName[name];
          let result;
          switch (field.type) {
            case 'text':
              result = <TextInput {...field} onChange={this.handleChange}/>;
              break;
            case 'select':
              result = <SelectInput {...field} onChange={this.handleChange}/>;
              break;
            case 'checkbox':
              result = <Checkbox {...field} onChange={this.handleChange}/>;
              break;
            default:
              result = <p key="invalid">Invalid input type!</p>;
              break;
          }
          return (
            <div key={field.name}>
              {result}
            </div>
          );
        })}
        <button onClick={this.handleSubmit}>
          Submit
        </button>
        <div>
          {JSON.stringify(this.state)}
        </div>
      </div>
    );
  }
}

export default Form;
